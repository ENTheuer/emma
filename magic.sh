#!/usr/bin/env bash

### This Script automagically installs my window Manager setup with a simplified autostart and without my changing wallpapers by daytime setup

# The Greeter
echo "Setting the Windowmanager up, please hold on..."
sleep 2
clear

# Prepare the files and start the installation process
chmod +x /home/alyss/emma/qtile/autostart.sh
sudo pacman -S - < /home/alyss/emma/qtile/pkglist.txt --needed --noconfirm

# Finishing up
cp -Rfv /home/alyss/emma/qtile /home/alyss/.config/
sleep 2
clear
echo "The setup is done"
