import os
import re
import socket
import subprocess
from libqtile.config import KeyChord, Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice
myConfig = "~/.config/qtile/config.py"    # The Qtile config file location

keys = [

         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod], "r",
             lazy.spawn("rofi -show drun"),
             desc='Rofi Run Launcher'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod], "x",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "y",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod, "control"], "q",
             lazy.spawn("gnome-session-quit --logout --no-prompt"),
             desc='Quit out of the GNOME Shell'
             ),
         Key([mod, "shift"], "w",
             lazy.to_screen(0),
             desc='Move focus to Screen 1'
             ),
         Key([mod, "shift"], "e",
             lazy.to_screen(1),
             desc='Move focus to Screen 2'
             ),

         Key([mod], "f",
             lazy.spawn("firefox"),
             desc='Firefox'
             ),
         Key([mod], "n",
             lazy.spawn("discord"),
             desc='Discord'
             ),
         Key([mod], "w",
             lazy.spawn("lowriter"),
             desc='Libre Office Writer'
             ),
         Key([mod], "p",
             lazy.spawn("pcmanfm"),
             desc='PCmanFM'
             ),
         Key([mod], "v",
             lazy.spawn("vlc"),
             desc='VLC Media Player'
             ),
         Key([mod], "g",
             lazy.spawn("gscreenshot"),
             desc='My Screenshot Tool of Choice'
             ),
         Key([mod], "d",
             lazy.spawn("deadbeef"),
             desc='The DeaDBeeF Music Player'
             ),
         Key([mod], "m",
             lazy.spawn("mailspring"),
             desc='My Email Client of Choice'
             ),

         # Gscreenshot
         KeyChord([mod], "g", [
             Key([], "s", lazy.spawn("flameshot gui")),
             Key([], "g", lazy.spawn("flameshot full -c -p /home/emma/Bilder/flameshots"))
         ]),

         # Emacs
         KeyChord([mod], "e", [
             Key([], "e", lazy.spawn("emacsclient -c -a 'emacs'")),
             Key([], "p", lazy.spawn("emacsclient -c -a 'emacs' ~/Projektordner/Hauptprojekt.org")),
             Key([], "q", lazy.spawn("emacsclient -c -a 'emacs' ~/.config/qtile/config.org"))
         ]),

         # Steam
         KeyChord([mod], "z", [
             ### Steam Runtime
             Key([], "z", lazy.spawn("steam")),

             ### Hollow Knight
             Key([], "h", lazy.spawn("steam steam://rungameid/367520")),

             #### Code Vein
             Key([], "c", lazy.spawn("steam steam://rungameid/678960")),

             ### Red Dead Redemption
             Key([], "r", lazy.spawn("steam steam://rungameid/1174180")),

             ### Souls Series
             KeyChord([], "d", [
                 Key([], "1", lazy.spawn("steam steam://rungameid/570940")),
                 Key([], "2", lazy.spawn("steam steam://rungameid/335300")),
                 Key([], "3", lazy.spawn("steam steam://rungameid/374320")),
                 Key([], "s", lazy.spawn("steam steam://rungameid/814380")),
                 Key([], "e", lazy.spawn("steam steam://rungameid/1245620"))
             ]),

             ### SAO Series
             KeyChord([], "s", [
                 Key([], "1", lazy.spawn("steam steam://rungameid/607890")),
                 Key([], "2", lazy.spawn("steam steam://rungameid/626690"))
             ]),

             ### Outlast Series
             KeyChord([], "o", [
                 Key([], "1", lazy.spawn("steam steam://rungameid/238320")),
                 Key([], "2", lazy.spawn("steam steam://rungameid/414700"))
             ]),


             ### Tomb Raider Series
             KeyChord([], "t", [
                 Key([], "1", lazy.spawn("steam steam://rungameid/203160")),
                 Key([], "2", lazy.spawn("steam steam://rungameid/391220")),
                 Key([], "3", lazy.spawn("steam steam://rungameid/750920"))
             ])
         ])
]

group_names = [("Term", {'layout': 'monadtall'}),
               ("Web", {'layout': 'monadtall'}),
               ("Chat", {'layout': 'monadtall'}),
               ("Dev", {'layout': 'monadtall'}),
               ("Game", {'layout': 'floating'}),
               ("Text", {'layout': 'monadtall'}),
               ("Music", {'layout': 'monadtall'}),
               ("Misc", {'layout': 'floating'}),
               ("Qbit", {'layout': 'monadtall'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 6,
                "border_focus": "e1acff",
                "border_normal": "1D2330"
                }

layouts = [
    # layout.MonadWide(**layout_theme),
    # layout.Bsp(**layout_theme),
    # layout.Stack(stacks=2, **layout_theme),
    # layout.Columns(**layout_theme),
    # layout.Tile(shift_windows=True, **layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Zoomy(**layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.Max(**layout_theme),
    # layout.Stack(**layout_theme, num_stacks=2),
    layout.MonadTall(**layout_theme),
    layout.Floating(**layout_theme)
]

colors = [["#282c34", "#282c34"], # panel background
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names
          ["#ff5555", "#ff5555"], # border line color for current tab
          ["#74438f", "#74438f"], # border line color for other tab and odd widgets
          ["#4f76c7", "#4f76c7"], # color for the even widgets
          ["#e1acff", "#e1acff"]] # window name

# Sets the format for Qtile's run prompt. The config doesn't work if this isn't set but I didn't want to make a new Source Block and a literrate explanation for this, since it's only a thing that makes stuff work
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

widget_defaults = dict(
    font="Mononoki Nerd Font",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)

extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.GroupBox(
                       font = "Mononoki Nerd Font",
                       fontsize = 9,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       rounded = False,
                       highlight_color = colors[1],
                       highlight_method = "line",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Mononoki Nerd Font",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 40,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.WindowName(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
              widget.TextBox(
                       text='',
                       background = colors[0],
                       foreground = colors[4],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.CPU(
                       foreground = colors[2],
                       background = colors[4],
                       padding = 3
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[4],
                       foreground = colors[5],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.Memory(
                       foreground = colors[2],
                       background = colors[5],
                       padding = 5
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.Net(
                       foreground = colors[2],
                       background = colors[4],
                       padding = 5
                       ),
              widget.TextBox(
                       text='',
                       background = colors[4],
                       foreground = colors[5],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = colors[2],
                       background = colors[5],
                       fontsize = 14
                       ),
              widget.CheckUpdates(
                       update_interval = 1800,
                       distro = "Arch",
                       display_format = "{updates} Updates",
                       foreground = colors[2],
                       execute = 'alacritty -e doas pacman -Syu',
                       background = colors[5]
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.CurrentLayout(
                       foreground = colors[2],
                       background = colors[4],
                       padding = 5
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[4],
                       foreground = colors[5],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.Clock(
                       foreground = colors[2],
                       background = colors[5],
                       execute = 'gnome-calendar',
                       format = "%A, %B %d  [ %H:%M:%S ]"
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[0],
                       background = colors[5]
                       ),
              widget.TextBox(
                       text = '',
                       background = colors[5],
                       foreground = colors[4],
                       padding = -5,
                       fontsize = 37
                       ),
              widget.Systray(
                       background = colors[4],
                       padding = 2
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 5,
                       foreground = colors[2],
                       background = colors[4]
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                 # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

# A list, which tells the mouse what to do when which button is pressed together with the mod key.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = False
bring_front_click = True
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules
])

auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.Popen([home + '/.config/qtile/autostart.sh'])

    # For running Qtile in GNOME
    def dbus_register():
        id = os.environ.get('DESKTOP_AUTOSTART_ID')

    if not id:
        return
    subprocess.Popen(['dbus-send',
                      '--session',
                      '--print-reply',
                      '--dest=org.gnome.SessionManager',
                      '/org/gnome/SessionManager',
                      'org.gnome.SessionManager.RegisterClient',
                      'string:qtile',
                      'string:' + id])

wmname = "qtile"
