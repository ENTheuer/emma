#!/usr/bin/env bash
setxkbmap -layout us -variant altgr-intl &
/usr/bin/lxpolkit &
/usr/lib/kdeconnectd &
flameshot &
/usr/lib/notification-daemon-1.0/notification-daemon &
picom &
feh --bg-scale --no-fehbg ~/emma/Backgrounds/589406.jpg 
